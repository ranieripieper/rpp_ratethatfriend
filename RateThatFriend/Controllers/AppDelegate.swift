//
//  AppDelegate.swift
//  RateThatFriend
//
//  Created by Gilson Gil on 9/8/15.
//  Copyright © 2015 doisdoissete. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
  var window: UIWindow?

  func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
    return true
  }
}
